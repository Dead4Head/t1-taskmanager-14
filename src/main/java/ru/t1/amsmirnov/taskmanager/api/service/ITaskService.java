package ru.t1.amsmirnov.taskmanager.api.service;

import ru.t1.amsmirnov.taskmanager.api.repository.ITaskRepository;
import ru.t1.amsmirnov.taskmanager.enumerated.Sort;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.model.Task;

import java.util.List;

public interface ITaskService extends ITaskRepository {

    Task createTask(String name, String description);

    List<Task> findAll(Sort sort);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeStatusById(String id, Status status);

    Task changeStatusByIndex(Integer index, Status status);

}
