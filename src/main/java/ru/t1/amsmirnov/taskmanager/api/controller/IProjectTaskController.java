package ru.t1.amsmirnov.taskmanager.api.controller;

public interface IProjectTaskController {

    void bindTaskToProject();

    void unbindTaskToProject();

}
