package ru.t1.amsmirnov.taskmanager.api.controller;

public interface IProjectController {

    void createProject();

    void showProjects();

    void showProjectById();

    void showProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

    void changeProjectStatusById();

    void changeProjectStatusByIndex();

    void startProjectById();

    void startProjectByIndex();

    void completeProjectById();

    void completeProjectByIndex();

    void clearProjects();

    void removeProjectById();

    void removeProjectByIndex();

}